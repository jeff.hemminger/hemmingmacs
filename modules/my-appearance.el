(require 'term)

(load-theme 'material)


(setq redisplay-dont-pause t)
(global-linum-mode t)


(hlinum-activate)

(setq column-number-mode t)
(which-function-mode)

(show-paren-mode 1)

(provide 'my-appearance)
