(use-package s)

(use-package yasnippet
  :config
  (yas-global-mode 1))

(provide 'hemminmacs-snippets)
