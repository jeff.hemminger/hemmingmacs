
(set-default 'indent-tabs-mode nil)

(setq sentence-end-double-space nil)

(define-key global-map (kbd "RET") 'newline-and-indent)

;(require 'ethan-wspace)

(global-ethan-wspace-mode 1)

(define-key global-map (kbd "C-c c") 'ethan-wspace-clean-all)

(setq mode-require-final-newline nil)

(setq require-final-newline nil)

(setq-default tab-width 2)

(provide 'hemminmacs-codestyle)

