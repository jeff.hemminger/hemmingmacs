
(highlight-parentheses-mode)

(defun hemminmacs/remove-elc-on-save ()
  "If you're saving an elisp file, likely the .elc is no longer valid."
  (make-local-variable 'after-save-hook)
  (add-hook 'after-save-hook
	    (lambda ()
	      (if (file-exists-p (concat buffer-file-name "c"))
		  (delete-file (concat buffer-file-name "c"))))))
(add-hook 'emacs-lisp-mode-hook 'hemminmacs/remove-elc-on-save)

;; enable eldoc-mode, which provides context-based documentation
;; in the minibuffer
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)

(add-hook 'emacs-lisp-mode-hook 'eros-mode)

(define-key emacs-lisp-mode-map (kbd "M-.") 'find-function-at-point)

(provide 'my-elisp)
