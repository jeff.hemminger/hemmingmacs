(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(load-library "iso-transl")

(defalias 'yes-or-no-p 'y-or-n-p)

(setq backup-directory-alist
      `(("." . ,(expand-file-name (concat dotfiles-dir "bak")))))

(setq compilation-ask-about-save nil)

(delete-selection-mode t)
(transient-mark-mode t)

(global-auto-revert-mode 1)

(setq compilation-scroll-output 'first-error)

(add-hook 'json-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c <tab>") 'json-mode-beautify)))

(with-current-buffer (get-buffer "*scratch*")
  (end-of-buffer)
  (delete-region 1 (point))
  (insert ";; Blessed art thou, who hath come to the One True Editor"))

(provide 'my-general)
