(defun spec-to-list (spec)
  (s-split "-" spec))

(defun list-to-spec (spec)
  (s-join "-" spec))

(defun update-font-spec-size (spec increment)
  (list-to-spec
   (-update-at 7 (lambda (i) (number-to-string
			      (+ (string-to-number i) increment)))
	       (spec-to-list spec))))

(defun update-font-size (increment)
  (set-frame-font
   (update-font-spec-size (frame-parameter nil 'font) increment)))

(global-set-key (kbd "C-M--") (lambda () (interactive)
				(update-font-size -1)))

(global-set-key (kbd "C-M-=") (lambda () (interactive)
				(update-font-size 1)))

(provide 'my-fonts)
