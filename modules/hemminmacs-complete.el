(global-company-mode)
(setq company-global-modes '(not term-mode))
(setq company-minimum-prefix-length 2
      company-selection-wrap-around t
      company-show-numbers t
      company-tooltip-align-annotations t
      company-require-match nil
      company-dabbrev-downcase nil
      company-dabbrev-ignore-case nil)
(setq company-transformers '(company-sort-by-occurrence))

(setq company-quickhelp-delay 1)
(company-quickhelp-mode 1)

;; TODO company-try-hard
(provide 'hemminmacs-complete)
