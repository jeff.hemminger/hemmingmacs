(global-set-key (kbd "M-n") 'next-error)
(global-set-key (kbd "M-p") 'previous-error)

(add-hook 'find-file-hook
          (lambda ()
            (when (not (equal 'emacs-lisp-mode major-mode))
              (flycheck-mode))))

(with-eval-after-load "flycheck"
  (setq flycheck-highlighting-mode 'symbols)
  (add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

(provide 'hemminmacs-flycheck)
