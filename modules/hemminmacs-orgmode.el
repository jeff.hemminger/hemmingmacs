(use-package org
  :ensure org-plus-contrib
  :config
  ;; stop org-mode from highjacking shift-cursor keys.
  (setq org-replace-disputed-keys t)
  ;; fancy bullet rendering
  (use-package org-bullets
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
  (use-package org-cliplink
    :config
    (with-eval-after-load "org"
      (define-key org-mode-map (kbd "C-c M-l" 'org-cliplink)))))

(provide 'hemminmacs-orgmode)
