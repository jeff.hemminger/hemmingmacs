(use-package emojify
  :config
  ;; Set emojify to only replace unicode emoji, and do it everywhere
  (setq emojify-emoji-style '(unicode)
        emojify-inhibiti-major-modes '())
  ;; Enable it globally
  (add-hook 'after-init-hook #'global-emojify-mode))

;; Patch emojify to replace emoji everywhere in programmining modes.
(defun emojify-valid-prog-context-p (beg end) 't)

(provide 'hemminmacs-emoji)
