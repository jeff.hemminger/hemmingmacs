(use-package unicode-fonts
  :config
  (unicode-fonts-setup))

(provide 'hemminmacs-unicode)
