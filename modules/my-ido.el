(ido-mode t)

(setq ido-enable-prefix nil
      ido-enable-flex-matching t
      ido-create-new-buffer 'always
      ido-use-filename-at-point 'guess
      ido-use-url-at-point t
      ido-max-prospects 10
      ido-use-virtual-buffers t)

;; make sure ido is everywhere
(ido-ubiquitous-mode 1)

;; use smex to provide ido-like interface for M-x
(use-package smex
  :config
  (smex-initialize)
  :bind (("M-X" . smex)
	 ("M-X" . smex-major-mode-commands)
	 ;; This is the old M-x
	 ("C-c C-c M-x" . execute-extended-command)))

(use-package ido-vertical-mode
  :config
  (ido-vertical-mode))

(use-package flx-ido
  :config
  (flx-ido-mode 1)
  (setq ido-enable-flex-matching t
	ido-use-faces nil
	gc-cons-threshold 20000000))

(provide 'my-ido)
