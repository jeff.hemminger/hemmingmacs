;; init.el
(when (or (< emacs-major-version 24) (and (= emacs-major-version 24) (< emacs-minor-version 4)))
  (x-popup-dialog  t `(, (format "Sorry, you need Gnu Emacs version 24.4 or higher to run this emacs config."))))

;; Figure out the path to our .emacs.d by getting the path part of the
;; current file (`init.el`).
(setq dotfiles-dir (file-name-directory
                    (or (buffer-file-name) (file-chase-links load-file-name))))

(setq debug-on-error t)

;;(setq package-check-signature nil)
(setq gc-cons-threshold 40000000)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")



;; Turn off mouse interface early in startup to avoid momentary display
(when window-system
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1))


;; Skip the splash screen
(setq inhibit-startup-message t)

(when (equal system-type 'darwin)
  (require 'cask)
  )

(cask-initialize)

;; Figure out the current hostname.
(setq hostname (replace-regexp-in-string "\\(^[[:space:]\n]*\\|[[:space:]\n]*$\\)" "" (with-output-to-string (call-process "hostname" nil standard-output))))


;; add package config files to system load path so emacs can find them
(add-to-list 'load-path (concat dotfiles-dir "modules"))

;; autoload declarations and custom el
(setq autoload-file (concat dotfiles-dir "loaddefs.el"))
(setq custom-file (concat dotfiles-dir "custom.el"))

(load custom-file 'noerror)

(setq
 hemminmacs/available-modules
 '((my-appearance "how emacs looks" :recommended)
   (my-fonts "adjust font sizes on the fly" :recommended)
   (my-general "basic editor settings" :recommended)
   (my-ido "improved file selector" :recommended)
   (my-elisp "Emacs Lisp" :recommended)
   (hemminmacs-codestyle "editing details" :recommended)
   (hemminmacs-complete "adding company completion" :recommended)
   (hemminmacs-flycheck "add error checking" :recommended)
   (hemminmacs-git "git porcelin" :recommended)
   (hemminmacs-project "projectile everything" :recommend)
   (hemminmacs-lsp "lsp mode" :recommend)
   (hemminmacs-markdown "markdown mode" :recommend)
   (hemminmacs-helm "helm mode" :recommend)
   (hemminmacs-snippets "snippets" :recommend)
   (hemminmacs-emoji "emoji" :recommend)
   (hemminmacs-unicode "unicode" :recommend)
   (hemminmacs-orgmode "org-mode" :recommend)
   (hemminmacs-json "json mode" :recommend)
   ))


(setq use-package-always-ensure t)

(require 'cl-lib)

(defcustom hemminmacs/modules (mapcar #'car
			   (cl-remove-if-not
			    (lambda (i) (equal :recommended (caddr i)))
			    hemminmacs/available-modules))
  "Your choice of modules.")

(defun hemminmacs/load-my-modules ()
  (interactive)
  (dolist (module hemminmacs/modules) (require module nil t)))

(hemminmacs/load-my-modules)
